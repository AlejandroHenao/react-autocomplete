import React from 'react'
class Sugestion  extends React.Component{
    render(){
        return (
            <div
                onClick={()=>this.props.clickSubmit(this.props.option)}
                className={this.props.sugestionClass+' '+this.props.keyhover}
            >
                {this.props.option}
            </div> 
        )     
    }
}
export default Sugestion 
